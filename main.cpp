#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <exception>
#include <list>
#include <vector>
#include <numeric>


int main(int argc, char ** argv) {
    std::vector<std::string> args;
    std::copy(argv, argv + argc, std::back_inserter(args));

    if (args.size() < 3) {
        std::cout<<"Usage:\n\t"<<args.front()<<" <file> <N> [<output file>]"<<std::endl;
        return -1;
    }
    std::string iFile {args[1]};
    size_t N;
    try{
        N = std::stoul(args[2]);

    } catch (...) {
        std::cout<<"Cannot convert "<<args[2]<<" to unsigned integer"<<std::endl;
        return -1;
    }

    std::string oFile {(args.size()>3?args[3]:iFile + ".out")};
    std::cout<<"Reading from: "<<iFile<<std::endl;
    std::cout<<"Output file: "<<oFile<<std::endl;
    std::cout<<"Window width: "<<N<<std::endl;
    std::cout<<std::endl; 

    std::ifstream ifs{iFile};
    if (!ifs.good()) {
        std::cout<<"Cannot open input file: \"" << iFile << "\""<<std::endl;
        return -1;
    }
    std::ofstream ofs{oFile};
    if (!ofs.good()) {
        std::cout<<"Cannot open output file: \"" << oFile << "\"" << std::endl;
        return -1;
    }

    // Read initial window and mirror it
    std::list<double> win;
    for (size_t i = 0; (i<N+1) && (!ifs.eof());) {
        std::string val;
        std::getline(ifs, val);
        try {
            double num {std::stod(val)};
            win.push_back(num);

        } catch (...) {
            continue;
        }
        ++i;
    }
    if (win.size() != N+1) {
        std::cout << "Expected at least " << (N+1) << " numbers, got " << win.size() << std::endl;
        return -1;
    }

    auto it = win.begin();
    ++it;
    while (it != win.end()) {
        win.push_front(*it);
        ++it;
    }

#ifdef FASTAVG
    double winSum = std::accumulate(win.begin(), win.end(), 0.0);
#endif
    size_t written = 0;
    // Process other values from file
    while (!ifs.eof()) {
#ifdef FASTAVG
        double curr = winSum / win.size();
#else
        double curr = std::accumulate(win.begin(), win.end(), 0.0) / win.size();
#endif


        ofs << curr << std::endl;
        ++written;
        std::string val;
        std::getline(ifs, val);
        try {
            double num{std::stod(val)};
            win.push_back(num);
#ifdef FASTAVG
            winSum -= win.front();
            winSum += win.back();
#endif
            win.pop_front();
            
        } catch (...) {
            continue;
        }
    }
    // Write tail values using mirroring
    auto cit = win.rbegin();
    ++cit;
    while (cit != win.rend()) {
        win.push_back(*cit);
#ifdef FASTAVG
        winSum -= win.front();
#endif
        
        win.pop_front();

#ifdef FASTAVG
        winSum += win.back();
        double curr = winSum/win.size();
#else
        double curr = std::accumulate(win.begin(), win.end(), 0.0) / win.size();
#endif
        ofs << curr << std::endl;
        ++written;
        ++cit;
    }
    std::cout << "Processed " << written << " numbers" << std::endl; 

    
    return 0;
}
